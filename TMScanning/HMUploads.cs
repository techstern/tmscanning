﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TMScanning.Infrastructure;
using System.Collections.Specialized;
using Ionic.Zip;
using System.Net.Http.Handlers;
using System.Net;

namespace TMScanning
{
    public partial class HMUploads : Form
    {
        Constant c = new Constant();
        AccessInfo aa = new AccessInfo();
        FileStatus f = new FileStatus();
        public DataGridView Dgv { get; set; }
        public Label page { get; set; }
        public HMUploads(FileStatus a)
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.StartPosition = FormStartPosition.CenterParent;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.ControlBox = false;
            this.BackColor = Color.FromArgb(255, 255, 255);
            if (a.IsActive)
            {
                aa.SchoolID = a.SchoolID;
                UploadFile();
            }
        }

        private void HttpSendProgress(object sender, HttpProgressEventArgs e)
        {
            HttpRequestMessage request = sender as HttpRequestMessage;
            UploadProgressBar(e);
        }

        delegate void UploadProgressCallback(HttpProgressEventArgs e);
        delegate void UpdateStatusCallback(string Status);

        private const long OneKb = 1024;
        private const long OneMb = OneKb * 1024;

        public string ToPrettySize(long? value, int decimalPlaces = 0)
        {
            var asMb = Math.Round((double)value / OneMb, decimalPlaces);
            var asKb = Math.Round((double)value / OneKb, decimalPlaces);
            string chosenValue =
                  asMb > 1 ? string.Format("{0} Mb", asMb)
                : asKb > 1 ? string.Format("{0} Kb", asKb)
                : string.Format("{0} B", Math.Round((double)value, decimalPlaces));
            return chosenValue;
        }
        private void UploadProgressBar(HttpProgressEventArgs e)
        {
            if (this.progressBar1.InvokeRequired)
            {
                UploadProgressCallback u = new UploadProgressCallback(UploadProgressBar);
                this.Invoke(u, new object[] { e });
            }
            else
            {
                lblTotal.Show();
                lblTransfer.Show();
                lblPerc.Show();
                lblMessage.Show();
                progressBar1.Show();
                btnCancel.Show();
                lblTotal.Text = "Total File Size : " + ToPrettySize(e.TotalBytes, 2);
                lblTransfer.Text = "Transfer : " + ToPrettySize(e.BytesTransferred, 2);
                lblPerc.Text = e.ProgressPercentage + "%";
                lblMessage.Text = "File Uploading Please Wait..";
                progressBar1.Value = e.ProgressPercentage;
            }
        }
        private void UpdateStatus(string Status)
        {
            if (this.lblZip.InvokeRequired)
            {
                UpdateStatusCallback u = new UpdateStatusCallback(UpdateStatus);
                this.Invoke(u, new object[] { Status });
            }
            else
            {
                if(Status== "Success")
                {
                    lblMessage.Show();
                    lblMessage.Text = "All Files Uploaded Successfully";
                    page.Text = "0";
                    btnCancel.Text = "Close";
                    Dgv.Rows.Clear();
                    DirectoryInfo di = new DirectoryInfo(ScanSetup.FileLocation);
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                }
            }
        }

        private async void UploadFile()
        {
            try
            {
                ProgressMessageHandler progress = new ProgressMessageHandler();
                progress.HttpSendProgress += new EventHandler<HttpProgressEventArgs>(HttpSendProgress);
                HttpRequestMessage message = new HttpRequestMessage();
                HttpClient httpClient = new HttpClient();
                lblZip.Show();
                lblZip.Text = "File Uploading Process wait..";
                string zipPath = await Task.Run(() => MakeZip());
                if (zipPath != null && zipPath!="")
                {
                    lblZip.Hide();
                    var fileStream = File.Open(zipPath, FileMode.Open);
                    var fileInfo = new FileInfo(zipPath);
                    FileUploadResult uploadResult = null;
                    string SchoolID = aa.SchoolID.ToString();
                    var content = new MultipartFormDataContent();
                    content.Add(new StreamContent(fileStream), "\"file\"", string.Format("\"{0}\"", SchoolID + ".zip"));

                    message.Method = HttpMethod.Post;
                    message.Content = content;
                    message.RequestUri = new Uri(c.uploadServiceBaseAddress);

                    var client = HttpClientFactory.Create(progress);

                    var ts = TimeSpan.FromMinutes(15);
                    client.Timeout.Add(ts);
                    Task taskUpload = client.SendAsync(message).ContinueWith(task =>
                    {
                        if (task.Status == TaskStatus.RanToCompletion)
                        {
                            var response = task.Result;
                            if (response.IsSuccessStatusCode)
                            {
                                uploadResult = response.Content.ReadAsAsync<FileUploadResult>().Result;
                                if (uploadResult.Message == "Success")
                                {
                                    c.message = uploadResult.Message;
                                    UpdateStatus(c.message);
                                }
                            }
                            else
                            {
                                MessageBox.Show("oops Something goes wrong Try again later..");
                            }
                        }
                        fileStream.Dispose();
                    });
                    httpClient.Dispose();
                }
                else
                {
                    lblZip.Text = "There is no scanning files in current directory..";
                    btnCancel.Show();
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.Timeout)
                {
                    MessageBox.Show("Your connection has been lost..");
                }
            }
        }


        public string MakeZip()
        {
            
            string fileName = "";
            string zipName = "";
            try
            {
                ZipFile zip = new ZipFile();
                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                string[] filePaths = Directory.GetFiles(ScanSetup.FileLocation, "*.jpeg");
                if (filePaths != null)
                {
                    foreach (var i in filePaths)
                    {
                        zip.AddFile(i, @"\");
                    }
                    if (zip.Count > 0)
                    {
                        zipName = String.Format("ScanImg_{0}.zip", DateTime.Now.ToString("yyyyMMddHHmmss"));
                        fileName = ScanSetup.FileLocation + zipName;
                        zip.Save(fileName);
                    }
                }
            }
            catch (Exception ex)
            { }
            return fileName;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

       
    }
}
